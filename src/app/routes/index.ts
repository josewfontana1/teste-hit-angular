import { Routes } from "@angular/router";
import { IntroductionComponent } from "../views/introduction/introduction.component";
import { ContactComponent } from "../views/contact/contact.component";

export const routes: Routes = [
  {
    path: '',
    component: IntroductionComponent,
    data: { title: 'Introdução' }
  },
  {
    path: 'contact',
    component: ContactComponent,
    data: { title: 'Contato' }
  }
]