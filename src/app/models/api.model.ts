type responseStatus = 'success' | 'error';

export interface ResponseData<TData> {
  status: responseStatus;
  data: TData;
}