export interface IntroductionResponse {
  title: string;
  text: string;
}