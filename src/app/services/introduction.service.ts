import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IntroductionResponse } from '../models/introduction.model';
import { ResponseData } from '../models/api.model';

@Injectable({
  providedIn: 'root'
})
export class IntroductionService {
  result: any;
  url: string = 'https://hitdigital.com.br/test.php';

  constructor(private http: HttpClient) { }

  getIntroduction(): Observable<ResponseData<IntroductionResponse>> {
    return this.http.get<ResponseData<IntroductionResponse>>(this.url)
  }
}
