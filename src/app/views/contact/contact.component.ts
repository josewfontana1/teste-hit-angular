import { Component } from '@angular/core';
import { CardComponent } from '../../components/card/card.component';
import { InputComponent } from '../../components/input/input.component';
import { TextareaComponent } from '../../components/textarea/textarea.component';
import { ButtonComponent } from '../../components/button/button.component';
import { NavheaderComponent } from '../../components/navheader/navheader.component';

@Component({
  selector: 'app-contact',
  standalone: true,
  imports: [CardComponent, InputComponent, TextareaComponent, ButtonComponent, NavheaderComponent],
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.scss'
})

export class ContactComponent {
  title = 'Contato'
}
