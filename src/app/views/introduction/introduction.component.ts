import { Component } from '@angular/core';
import { CardComponent } from '../../components/card/card.component';
import { NavheaderComponent } from '../../components/navheader/navheader.component';
import { ButtonComponent } from '../../components/button/button.component';
import { IntroductionService } from '../../services/introduction.service';
import { IntroductionResponse } from '../../models/introduction.model';

@Component({
  selector: 'app-introduction',
  standalone: true,
  imports: [CardComponent, NavheaderComponent, ButtonComponent],
  templateUrl: './introduction.component.html',
  styleUrl: './introduction.component.scss'
})

export class IntroductionComponent {
  resultData!: IntroductionResponse;
  title = 'Introdução'

  constructor(
    private introductionService: IntroductionService
  ){}

  async ngOnInit() {
    this.introductionService.getIntroduction().subscribe(result => this.resultData = result.data);
  }
}
