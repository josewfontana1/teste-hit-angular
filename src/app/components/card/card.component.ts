import { Component } from '@angular/core';

@Component({
  selector: 'card-component',
  styleUrl: './card.component.scss',
  standalone: true,
  imports: [],
  template: `
    <div class="container-card">
      <ng-content></ng-content>
    </div>
  `
})
export class CardComponent {

}
