import { Component, Input } from '@angular/core';

@Component({
  selector: 'input-component',
  templateUrl: './input.component.html',
  standalone: true,
  imports: [],
  styleUrl: './input.component.scss'
})

export class InputComponent {
  @Input() placeholder = ''
}
