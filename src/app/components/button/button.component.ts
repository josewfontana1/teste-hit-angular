import { NgIf } from '@angular/common';
import { Component, Input, input } from '@angular/core';
import { RouterLink } from '@angular/router';
import { ButtonType } from './buttonTypes';

@Component({
  selector: 'button-component',
  standalone: true,
  imports: [RouterLink, NgIf],
  templateUrl: './button.component.html',
  styleUrl: './button.component.scss'
})

export class ButtonComponent {
  @Input() text: string = '';
  @Input() link: string | null = null;
  @Input() type: ButtonType = 'primary'

  buttonClass: string = 'button-primary'

  ngOnInit() {
    this.buttonClass = `button-${this.type}`
  }
}
