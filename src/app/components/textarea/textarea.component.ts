import { Component, Input } from '@angular/core';

@Component({
  selector: 'textarea-component',
  standalone: true,
  imports: [],
  templateUrl: './textarea.component.html',
  styleUrl: './textarea.component.scss'
})
export class TextareaComponent {
  @Input() placeholder = ''
}
