import { Component } from '@angular/core';
import { ButtonComponent } from '../button/button.component';

@Component({
  selector: 'navheader-component',
  standalone: true,
  imports: [ ButtonComponent ],
  templateUrl: './navheader.component.html',
  styleUrl: './navheader.component.scss'
})
export class NavheaderComponent {

}
